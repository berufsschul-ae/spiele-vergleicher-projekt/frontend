FROM nginx:stable

RUN rm -rf /usr/share/nginx/html
COPY . /usr/share/nginx/html
COPY ./entrypoint.sh /

EXPOSE 80 
ENTRYPOINT /entrypoint.sh
