function setUpWebsocket(user) {
    var socket = new SockJS(`${config.backendIp}event`);
    stompClient = Stomp.over(socket);
    stompClient.connect({}, frame => {
      //console.log("Connected: " + frame);
      stompClient.subscribe("/r/" + user.lobId, (message) => {
        msg = JSON.parse(message.body);
        switch(msg.event) {
            case "CONNECT":
                if(msg.userUuid != user.id && $(`#${msg.userUuid}`).length == 0){
                    addUserToList(msg.payload, msg.userUuid);
                }
                console.log(msg.payload + " connected");
                break;
            case "READY_STATE_CHANGE":
                if(msg.payload === 'true') {
                    console.log("Changing to Ready");
                    $(`#${msg.userUuid}`).children(".user_icon")
                                                .removeClass("not_ready")
                                                .addClass("ready");
                } else {
                  console.log("Changing to Unready");
                    $(`#${msg.userUuid}`).children(".user_icon")
                                                .removeClass("ready")
                                                .addClass("not_ready");
                }
                console.log(msg.userUuid + " changed his state to " + msg.payload);
                break;
            case "ALL_READY":
                $.get({
                  url: `${config.backendIp}api/lobby/${lobbyId}/result`,
                  success: (data) => {
                    console.log(data);
                    createResultList(data);

                    stompClient.disconnect();
                  }
                });
                console.log("Lobby " + msg.userUuid +  " is marked as ready");
                break;
            case "ERROR":
                console.log(msg.userId + " experienced an error");
                break;
            case "DISCONNECT":
                $(`.${msg.userUuid}`).remove();
                console.log(msg.userUuid + " discconected");
                break;
            default:
                break;
        }
      })

      // user custom channel
      // mainly used for pong responses
      //stompClient.subscribe("/r/" + user.id, (message) => {
      //})

      let obj = {userUuid: user.id, event: "CONNECT", payload: user.name}

      sendToLobby(obj);
      
      setInterval(() => {
        sendPing();
      }, 10000);
    })

    onbeforeunload = () => {
      stompClient.disconnect();
    }

    function sendPing(){
      stompClient.send(`/ws/${user.lobId}/${user.id}/ping`,{}, "ping");
    }

    function sendToLobby(payload){
      stompClient.send(`/ws/${user.lobId}`, {}, JSON.stringify(payload));
    }
  }