#!/bin/sh

sed -i "s/localhost:8080/${GAME_DB_URL}/g" /usr/share/nginx/html/configuration/config.json 
sed -i "s/localhost:8181/${BACKEND_URL}/g" /usr/share/nginx/html/configuration/config.json 

exec nginx -g 'daemon off;' "$@"
